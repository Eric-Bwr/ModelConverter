#include "Processor.h"

static void writeMatrix(const Mat4f& matrix, Binary_buffer* binaryBuffer){
    binary_buffer_push_float(binaryBuffer, matrix.m00);
    binary_buffer_push_float(binaryBuffer, matrix.m01);
    binary_buffer_push_float(binaryBuffer, matrix.m02);
    binary_buffer_push_float(binaryBuffer, matrix.m03);

    binary_buffer_push_float(binaryBuffer, matrix.m10);
    binary_buffer_push_float(binaryBuffer, matrix.m11);
    binary_buffer_push_float(binaryBuffer, matrix.m12);
    binary_buffer_push_float(binaryBuffer, matrix.m13);

    binary_buffer_push_float(binaryBuffer, matrix.m20);
    binary_buffer_push_float(binaryBuffer, matrix.m21);
    binary_buffer_push_float(binaryBuffer, matrix.m22);
    binary_buffer_push_float(binaryBuffer, matrix.m23);

    binary_buffer_push_float(binaryBuffer, matrix.m30);
    binary_buffer_push_float(binaryBuffer, matrix.m31);
    binary_buffer_push_float(binaryBuffer, matrix.m32);
    binary_buffer_push_float(binaryBuffer, matrix.m33);
}

static void writeVec3(const Vec3f& vector, Binary_buffer* binaryBuffer){
    binary_buffer_push_float(binaryBuffer, vector.x);
    binary_buffer_push_float(binaryBuffer, vector.y);
    binary_buffer_push_float(binaryBuffer, vector.z);
}

static void writeQuat(const Quatf& quat, Binary_buffer* binaryBuffer){
    binary_buffer_push_float(binaryBuffer, quat.x);
    binary_buffer_push_float(binaryBuffer, quat.y);
    binary_buffer_push_float(binaryBuffer, quat.z);
    binary_buffer_push_float(binaryBuffer, quat.w);
}

static void writeString(const std::string& string, Binary_buffer* binaryBuffer){
    uint32_t size = string.size();
    binary_buffer_push32(binaryBuffer, size);
    binary_buffer_push_string(binaryBuffer, string.data());
}

static void writeArray(const std::vector<float>& array, Binary_buffer* binaryBuffer){
    uint64_t size = array.size();
    binary_buffer_push64(binaryBuffer, size);
    for(float element : array)
        binary_buffer_push_float(binaryBuffer, element);
}

static void writeArray(const std::vector<unsigned int>& array, Binary_buffer* binaryBuffer){
    uint64_t size = array.size();
    binary_buffer_push64(binaryBuffer, size);
    for(uint64_t element : array)
        binary_buffer_push64(binaryBuffer, element);
}

static void writeAnimationMaps(const std::unordered_map<std::string, std::map<float, Vec3f>>& map, Binary_buffer* binaryBuffer){
    uint64_t size = map.size();
    binary_buffer_push64(binaryBuffer, size);
    for(const auto& element : map){
        size = element.second.size();
        binary_buffer_push64(binaryBuffer, size);
        writeString(element.first, binaryBuffer);
        for(const auto& nestedElement : element.second){
            auto key = (float)nestedElement.first;
            binary_buffer_push_float(binaryBuffer, key);
            writeVec3(nestedElement.second, binaryBuffer);
        }
    }
}

static void writeAnimationMaps(const std::unordered_map<std::string, std::map<float, Quatf>>& map, Binary_buffer* binaryBuffer){
    uint32_t size = map.size();
    binary_buffer_push64(binaryBuffer, size);
    for(const auto& element : map){
        size = element.second.size();
        binary_buffer_push64(binaryBuffer, size);
        writeString(element.first, binaryBuffer);
        for(const auto& nestedElement : element.second){
            auto key = (float)nestedElement.first;
            binary_buffer_push_float(binaryBuffer, key);
            writeQuat(nestedElement.second, binaryBuffer);
        }
    }
}

static void writeNode(const ResourceAssimpNodeData& node, Binary_buffer* binaryBuffer){
    writeString(node.name, binaryBuffer);
    writeMatrix(node.transformation, binaryBuffer);
    uint32_t size = node.children.size();
    binary_buffer_push32(binaryBuffer, size);
    for(const auto& child : node.children)
        writeNode(child, binaryBuffer);
}

void writeResourceSceneData(ResourceSceneData& resourceSceneData, Binary_buffer* binaryBuffer){
    writeNode(resourceSceneData.node, binaryBuffer);
    uint32_t numMaterials = resourceSceneData.materials.size();
    uint32_t numMeshes = resourceSceneData.meshes.size();
    uint32_t numTextures = resourceSceneData.textures.size();
    uint32_t numAnimations = resourceSceneData.animations.size();
    uint32_t numSkeletons = resourceSceneData.skeletons.size();
    std::cout << "\nMaterials: " << numMaterials << "\n";
    std::cout << "Meshes: " << numMeshes << "\n";
    std::cout << "Animations: " << numAnimations << "\n";
    std::cout << "Skeletons: " << numSkeletons << "\n";
    binary_buffer_push32(binaryBuffer, numMaterials);
    binary_buffer_push32(binaryBuffer, numTextures);
    binary_buffer_push32(binaryBuffer, numMeshes);
    binary_buffer_push32(binaryBuffer, numAnimations);
    binary_buffer_push32(binaryBuffer, numSkeletons);
    for(int i = 0; i < numMaterials; i++){
        auto material = resourceSceneData.materials[i];
        writeVec3(material.diffuse, binaryBuffer);
        writeVec3(material.specular, binaryBuffer);
        writeVec3(material.emissive, binaryBuffer);
        writeVec3(material.ambient, binaryBuffer);
        binary_buffer_push_float(binaryBuffer, material.shininess);
    }
    for(int i = 0; i < numTextures; i++){
        auto texture = resourceSceneData.textures[i];
        uint32_t size = texture.paths.size();
        binary_buffer_push32(binaryBuffer, size);
        for(int j = 0; j < size; j++)
            writeString(texture.paths[j], binaryBuffer);
    }
    for(int i = 0; i < numMeshes; i++){
        auto mesh = resourceSceneData.meshes[i];
        writeMatrix(mesh.transformation, binaryBuffer);
        binary_buffer_push32(binaryBuffer, mesh.materialIndex);
        writeArray(mesh.vertices, binaryBuffer);
        writeArray(mesh.textureCoords, binaryBuffer);
        writeArray(mesh.normals, binaryBuffer);
        writeArray(mesh.tangents, binaryBuffer);
        writeArray(mesh.bitangents, binaryBuffer);
        writeArray(mesh.boneIDs, binaryBuffer);
        writeArray(mesh.weights, binaryBuffer);
        writeArray(mesh.indices, binaryBuffer);
    }
    for(const auto& animation : resourceSceneData.animations){
        writeString(animation.first, binaryBuffer);
        auto animationData = animation.second;
        auto data = (float)animationData.duration;
        binary_buffer_push_float(binaryBuffer, data);
        data = (float)animationData.ticksPerSecond;
        binary_buffer_push_float(binaryBuffer, data);
        writeNode(animationData.nodeHierachy, binaryBuffer);
        writeAnimationMaps(animationData.keyPositions, binaryBuffer);
        writeAnimationMaps(animationData.keyRotations, binaryBuffer);
        writeAnimationMaps(animationData.keyScales, binaryBuffer);
    }
    for(const auto& skeleton : resourceSceneData.skeletons){
        writeString(skeleton.first, binaryBuffer);
        uint32_t size = skeleton.second.size();
        binary_buffer_push64(binaryBuffer, size);
        for(const auto& nestedMap : skeleton.second){
            writeString(nestedMap.first, binaryBuffer);
            auto boneData = nestedMap.second;
            uint64_t data = boneData.boneID;
            binary_buffer_push64(binaryBuffer, data);
            writeMatrix(boneData.nodeTransform, binaryBuffer);
            writeMatrix(boneData.offset, binaryBuffer);
        }
    }
}