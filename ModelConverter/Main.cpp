#include "Loader.h"
#include "Processor.h"

int errorCount = 0;

void convertModel(std::string &file) {
    std::cout << "\nProcessing " << file << "...\n";
    std::string toProcess("Input/");
    toProcess.append(file);
    Assimp::Importer importer;
    importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
    importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_READ_LIGHTS, false);
    importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_READ_CAMERAS, false);
    const aiScene *scene = importer.ReadFile(toProcess.c_str(),
                                             aiProcess_Triangulate |
                                             aiProcess_GenNormals |
                                             aiProcess_OptimizeGraph |
                                             aiProcess_JoinIdenticalVertices |
                                             aiProcess_ImproveCacheLocality |
                                             aiProcess_FlipUVs |
                                             aiProcess_CalcTangentSpace);
    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        std::cout << "Failed to load Model: " << file << std::endl;
        std::cout << importer.GetErrorString() << std::endl;
        errorCount += 1;
    } else {
        ResourceSceneData resourceSceneData;
        resourceSceneData.meshes.resize(scene->mNumMeshes);
        resourceSceneData.materials.resize(scene->mNumMaterials);
        resourceSceneData.textures.resize(scene->mNumMaterials);
        std::string filename("Output/");
        filename += getFilename((char *) file.data());
        filename = filename.substr(0, filename.find_last_of('.'));
        filename += ".bmf";
        Loader::loadScene(scene, resourceSceneData);
        Binary_buffer binaryBuffer = {};
        binary_buffer_create(&binaryBuffer, 1000000000);
        writeResourceSceneData(resourceSceneData, &binaryBuffer);
        binary_buffer_write_to_file(&binaryBuffer, filename.data());
        binary_buffer_destroy(&binaryBuffer);
    }
}

int main(int argc, char *argv[]) {
    std::vector<std::string> files = getFilenames(argv[0]);
    if (files.empty()) {
        std::cout << "Failed to load Files" << std::endl;
        system("pause");
        return 0;
    }
    for (auto file : files)
        convertModel(file);
    std::cout << "Finished with " << errorCount << " Errors." << std::endl;
    system("pause");
}