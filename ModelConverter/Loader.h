#pragma once

#include "Defines.h"

class Loader {
public:
    static void loadScene(const aiScene* scene, ResourceSceneData& resourceSceneData);
    static void loadNode(const aiScene* scene, const aiNode* node, ResourceSceneData& resourceSceneData, ResourceAssimpNodeData& resourceAssimpNodeData);

    static void loadMesh(const aiMesh *mesh, ResourceMeshData &resourceMeshData);
    static void loadMaterial(const aiMaterial* material, ResourceMaterialData& resourceMaterialData);
    static void loadTextures(const aiMaterial *material, ResourceTextureData &resourceTextureData);
    static void loadBoneData(const aiScene* scene, unsigned int meshIndex, std::map<std::string, AssimpBone>& data);
    static void loadHierachyBones(const aiNode* node, std::unordered_map<std::string, ResourceBoneData>& skeleton, std::map<std::string, AssimpBone>& bonesMap);
    static void loadAnimation(const aiAnimation* animation, ResourceAnimationData& resourceAnimationData);
};