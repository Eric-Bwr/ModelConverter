#include "Loader.h"

static std::string convertTextureType(int type){
    switch(type){
        case aiTextureType_DIFFUSE:
            return "Diffuse";
        case aiTextureType_SPECULAR:
            return "Specular";
        case aiTextureType_AMBIENT:
            return "Ambient";
        case aiTextureType_EMISSIVE:
            return "Emissive";
        case aiTextureType_HEIGHT:
            return "Height";
        case aiTextureType_NORMALS:
            return "Normal";
        case aiTextureType_SHININESS:
            return "Shininess";
        case aiTextureType_OPACITY:
            return "Opacity";
        case aiTextureType_DISPLACEMENT:
            return "Displacement";
        case aiTextureType_LIGHTMAP:
            return "Lightmap";
        case aiTextureType_REFLECTION:
            return "Reflection";
        default:
            return "Unknown";
    }
}

static Mat4f convertMatrix(const aiMatrix4x4 &matrix) {
    Mat4f result;
    result.m00 = matrix.a1;
    result.m10 = matrix.a2;
    result.m20 = matrix.a3;
    result.m30 = matrix.a4;

    result.m01 = matrix.b1;
    result.m11 = matrix.b2;
    result.m21 = matrix.b3;
    result.m31 = matrix.b4;

    result.m02 = matrix.c1;
    result.m12 = matrix.c2;
    result.m22 = matrix.c3;
    result.m32 = matrix.c4;

    result.m03 = matrix.d1;
    result.m13 = matrix.d2;
    result.m23 = matrix.d3;
    result.m33 = matrix.d4;
    return result;
}

void Loader::loadScene(const aiScene *scene, ResourceSceneData &resourceSceneData) {
    loadNode(scene, scene->mRootNode, resourceSceneData, resourceSceneData.node);
    if (scene->HasAnimations()) {
        for (auto i = 0; i < scene->mNumAnimations; i++) {
            auto animation = scene->mAnimations[i];
            loadAnimation(animation, resourceSceneData.animations[animation->mName.data]);
            resourceSceneData.animations[animation->mName.data].nodeHierachy = resourceSceneData.node;
        }
    }
}

void Loader::loadNode(const aiScene *scene, const aiNode *node, ResourceSceneData &resourceSceneData, ResourceAssimpNodeData &resourceAssimpNodeData) {
    for (int i = 0; i < node->mNumMeshes; i++) {
        auto meshIndex = node->mMeshes[i];
        const aiMesh *mesh = scene->mMeshes[meshIndex];
        auto finalTransform = node->mTransformation;
        if(node->mParent != nullptr)
            finalTransform = node->mTransformation * node->mParent->mTransformation;
        resourceSceneData.meshes[meshIndex].transformation = convertMatrix(finalTransform);
        auto materialIndex = mesh->mMaterialIndex;
        resourceSceneData.meshes[meshIndex].materialIndex = materialIndex;
        auto material = scene->mMaterials[materialIndex];
        loadMesh(mesh, resourceSceneData.meshes[meshIndex]);
        std::cout << "Mesh " << i << ":\n";
        std::cout << "Vertices: " << resourceSceneData.meshes[i].vertices.size() << "\n";
        std::cout << "Indices: " << resourceSceneData.meshes[i].indices.size() << "\n";
        loadMaterial(material, resourceSceneData.materials[materialIndex]);
        loadTextures(material, resourceSceneData.textures[materialIndex]);
        if (mesh->mNumBones > 0) {
            std::map<std::string, AssimpBone> bonesMap;
            loadBoneData(scene, meshIndex, bonesMap);
            if (resourceSceneData.skeletons.find(mesh->mName.data) == resourceSceneData.skeletons.end())
                loadHierachyBones(scene->mRootNode, resourceSceneData.skeletons[mesh->mName.data], bonesMap);
            auto vertexCount = resourceSceneData.meshes[meshIndex].vertices.size() / 3;
            resourceSceneData.meshes[meshIndex].boneIDs.resize(vertexCount * 4);
            resourceSceneData.meshes[meshIndex].weights.resize(vertexCount * 4);
            std::vector<BoneData> bonesData(vertexCount);
            for (auto it : bonesMap) {
                for (int j = 0; j < it.second.weights.size(); j++)
                    bonesData[it.second.vertexIDs[j]].addData(it.second.index, it.second.weights[j]);
            }
            bool triggeredWarning = false;
            for (auto j = 0; j < bonesData.size(); j++) {
                auto vertexID = j * 4;
                int k;
                if(bonesData[j].boneIDs.size() > 4 && !triggeredWarning) {
                    std::cout << "Warning: More than 4 Bones per Vertex\n";
                    triggeredWarning = true;
                }
                for (k = 0; k < bonesData[j].boneIDs.size() && k < 4; k++) {
                    resourceSceneData.meshes[meshIndex].boneIDs[vertexID + k] = bonesData[j].boneIDs[k];
                    resourceSceneData.meshes[meshIndex].weights[vertexID + k] = bonesData[j].weights[k];
                }
                for (; k < 4; k++) {
                    resourceSceneData.meshes[meshIndex].boneIDs[vertexID + k] = 0;
                    resourceSceneData.meshes[meshIndex].weights[vertexID + k] = 0;
                }
            }
        }
    }
    resourceAssimpNodeData.name = node->mName.C_Str();
    resourceAssimpNodeData.transformation = convertMatrix(node->mTransformation);
    resourceAssimpNodeData.children.resize(node->mNumChildren);
    for (auto i = 0; i < node->mNumChildren; i++)
        loadNode(scene, node->mChildren[i], resourceSceneData, resourceAssimpNodeData.children[i]);
}

void Loader::loadMesh(const aiMesh *mesh, ResourceMeshData &resourceMeshData) {
    auto verticesCount = mesh->mNumVertices;
    resourceMeshData.vertices.resize(verticesCount * 3);
    resourceMeshData.textureCoords.resize(verticesCount * 2);
    resourceMeshData.normals.resize(verticesCount * 3);
    resourceMeshData.tangents.resize(verticesCount * 3);
    resourceMeshData.bitangents.resize(verticesCount * 3);
    for (unsigned int i = 0; i < verticesCount; i++) {
        auto tri = (i * 3);
        auto du = (i * 2);
        resourceMeshData.vertices[tri + 0] = mesh->mVertices[i].x;
        resourceMeshData.vertices[tri + 1] = mesh->mVertices[i].y;
        resourceMeshData.vertices[tri + 2] = mesh->mVertices[i].z;
        if (mesh->mTextureCoords[0]) {
            resourceMeshData.textureCoords[du + 0] = mesh->mTextureCoords[0][i].x;
            resourceMeshData.textureCoords[du + 1] = mesh->mTextureCoords[0][i].y;
        } else {
            resourceMeshData.textureCoords[du + 0] = 0.0f;
            resourceMeshData.textureCoords[du + 1] = 0.0f;
        }
        resourceMeshData.normals[tri + 0] = mesh->mNormals[i].x;
        resourceMeshData.normals[tri + 1] = mesh->mNormals[i].y;
        resourceMeshData.normals[tri + 2] = mesh->mNormals[i].z;
        if (mesh->mTangents) {
            resourceMeshData.tangents[tri + 0] = mesh->mTangents[i].x;
            resourceMeshData.tangents[tri + 1] = mesh->mTangents[i].y;
            resourceMeshData.tangents[tri + 2] = mesh->mTangents[i].z;
        } else {
            resourceMeshData.tangents[tri + 0] = 0.0f;
            resourceMeshData.tangents[tri + 1] = 0.0f;
            resourceMeshData.tangents[tri + 2] = 0.0f;
        }
        if (mesh->mBitangents) {
            resourceMeshData.bitangents[tri + 0] = mesh->mBitangents[i].x;
            resourceMeshData.bitangents[tri + 1] = mesh->mBitangents[i].y;
            resourceMeshData.bitangents[tri + 2] = mesh->mBitangents[i].z;
        } else {
            resourceMeshData.bitangents[tri + 0] = 0.0f;
            resourceMeshData.bitangents[tri + 1] = 0.0f;
            resourceMeshData.bitangents[tri + 2] = 0.0f;
        }
    }
    resourceMeshData.indices.resize(mesh->mNumFaces * 3);
    for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
        auto tri = (i * 3);
        auto face = mesh->mFaces[i];
        if (face.mNumIndices != 3)
            std::cout << "Indices not equals 3\n";
        resourceMeshData.indices[tri + 0] = face.mIndices[0];
        resourceMeshData.indices[tri + 1] = face.mIndices[1];
        resourceMeshData.indices[tri + 2] = face.mIndices[2];
    }
}

void Loader::loadMaterial(const aiMaterial *material, ResourceMaterialData &resourceMaterialData) {
    aiColor3D color;
    material->Get(AI_MATKEY_COLOR_DIFFUSE, color);
    resourceMaterialData.diffuse = Vec3f(color.r, color.g, color.b);
    material->Get(AI_MATKEY_COLOR_AMBIENT, color);
    resourceMaterialData.ambient = Vec3f(color.r, color.g, color.b);
    material->Get(AI_MATKEY_COLOR_SPECULAR, color);
    resourceMaterialData.specular = Vec3f(color.r, color.g, color.b);
    material->Get(AI_MATKEY_COLOR_EMISSIVE, color);
    resourceMaterialData.emissive = Vec3f(color.r, color.g, color.b);
    float shininessStrength = 1.0f;
    material->Get(AI_MATKEY_SHININESS_STRENGTH, shininessStrength);
    resourceMaterialData.specular *= shininessStrength;
    material->Get(AI_MATKEY_SHININESS, shininessStrength);
    resourceMaterialData.shininess = shininessStrength;
}

void Loader::loadTextures(const aiMaterial *material, ResourceTextureData &resourceTextureData) {
    for (int i = 1; i < 12; i++) {
        auto type = aiTextureType(i);
        aiString path;
        material->GetTexture(type, 0, &path, nullptr, nullptr, nullptr, nullptr, nullptr);
        std::string stringPath = path.data;
        if (!stringPath.empty()){
            if(resourceTextureData.paths.empty())
                resourceTextureData.paths.resize(11);
            stringPath = stringPath.substr(stringPath.find_last_of('/') + 1, stringPath.size());
            std::cout << "Using Texture " << stringPath << " type: " << convertTextureType(type) << "\n";
            resourceTextureData.paths[i - 1] = stringPath;
        }
    }
}

void Loader::loadBoneData(const aiScene *scene, unsigned int meshIndex, std::map<std::string, AssimpBone> &data) {
    auto numBones = scene->mMeshes[meshIndex]->mNumBones;
    std::cout << "Bones: " << numBones << "\n";
    for (int i = 0; i < numBones; i++) {
        const aiBone *bone = scene->mMeshes[meshIndex]->mBones[i];
        auto boneName = bone->mName.C_Str();
        data[boneName].index = i;
        data[boneName].offset = convertMatrix(bone->mOffsetMatrix);
        data[boneName].weights.resize(bone->mNumWeights);
        data[boneName].vertexIDs.resize(bone->mNumWeights);
        for (auto j = 0; j < bone->mNumWeights; j++) {
            auto &vertexWeight = bone->mWeights[j];
            data[boneName].weights[j] = vertexWeight.mWeight;
            data[boneName].vertexIDs[j] = vertexWeight.mVertexId;
        }
    }
}

void Loader::loadHierachyBones(const aiNode *node, std::unordered_map<std::string, ResourceBoneData> &skeleton, std::map<std::string, AssimpBone> &bonesMap) {
    auto it = bonesMap.find(node->mName.data);
    if (it != bonesMap.end()) {
        ResourceBoneData resourceBoneData;
        resourceBoneData.boneID = it->second.index;
        resourceBoneData.name = it->first;
        resourceBoneData.nodeTransform = convertMatrix(node->mTransformation);
        resourceBoneData.offset = it->second.offset;
        auto itParent = bonesMap.find(node->mParent->mName.data);
        skeleton[resourceBoneData.name] = std::move(resourceBoneData);
    }
    for (auto i = 0; i < node->mNumChildren; i++)
        loadHierachyBones(node->mChildren[i], skeleton, bonesMap);
}

void Loader::loadAnimation(const aiAnimation *animation, ResourceAnimationData &resourceAnimationData) {
    resourceAnimationData.duration = animation->mDuration;
    resourceAnimationData.ticksPerSecond = animation->mTicksPerSecond;
    for (auto i = 0; i < animation->mNumChannels; i++) {
        auto nodeAnim = animation->mChannels[i];
        auto channelName = nodeAnim->mNodeName.data;
        for (auto key = 0; key < nodeAnim->mNumPositionKeys; key++) {
            auto vectorKey = nodeAnim->mPositionKeys[key];
            resourceAnimationData.keyPositions[channelName][vectorKey.mTime] = Vec3f(vectorKey.mValue.x, vectorKey.mValue.y, vectorKey.mValue.z);
        }
        for (auto key = 0; key < nodeAnim->mNumRotationKeys; key++) {
            auto vectorKey = nodeAnim->mRotationKeys[key];
            resourceAnimationData.keyRotations[channelName][vectorKey.mTime] = Quatf(vectorKey.mValue.x, vectorKey.mValue.y, vectorKey.mValue.z, vectorKey.mValue.w);
        }
        for (auto key = 0; key < nodeAnim->mNumScalingKeys; key++) {
            auto vectorKey = nodeAnim->mScalingKeys[key];
            resourceAnimationData.keyScales[channelName][vectorKey.mTime] = Vec3f(vectorKey.mValue.x, vectorKey.mValue.y, vectorKey.mValue.z);
        }
    }
}