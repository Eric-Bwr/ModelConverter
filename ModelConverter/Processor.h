#pragma once

#include "Defines.h"

void writeResourceSceneData(ResourceSceneData& resourceSceneData, Binary_buffer* binaryBuffer);
void clearResourceSceneData(ResourceSceneData& resourceSceneData);