#pragma once

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <map>
#include <unordered_map>
#include <vector>
#include <string>
#include <iostream>
#include "Util/FileStream.h"
#include "Util/Matrix.h"
#include "Util/Vector.h"
#include "Util/Quaternion.h"

struct ResourceMaterialData{
    Vec3f diffuse;
    Vec3f specular;
    Vec3f emissive;
    Vec3f ambient;
    float shininess;
};

struct ResourceTextureData{
    std::vector<std::string> paths;
};

struct ResourceMeshData{
    Mat4f transformation;
    unsigned int materialIndex;
    std::vector<float> vertices;
    std::vector<float> textureCoords;
    std::vector<float> normals;
    std::vector<float> tangents;
    std::vector<float> bitangents;
    std::vector<float> boneIDs;
    std::vector<float> weights;
    std::vector<unsigned int> indices;
};

struct ResourceAssimpNodeData {
    std::string name;
    Mat4f transformation;
    std::vector<ResourceAssimpNodeData> children;
};

struct ResourceBoneData {
    unsigned int boneID;
    std::string name;
    Mat4f nodeTransform = identityMatrix();
    Mat4f offset = identityMatrix();
};

struct ResourceAnimationData {
    double duration = 0.0;
    double ticksPerSecond = 0.0;
    std::unordered_map<std::string, std::map<float, Vec3f>> keyPositions;
    std::unordered_map<std::string, std::map<float, Quatf>> keyRotations;
    std::unordered_map<std::string, std::map<float, Vec3f>> keyScales;
    ResourceAssimpNodeData nodeHierachy;
};

struct ResourceSceneData {
    ResourceAssimpNodeData node;
    std::vector<ResourceMeshData> meshes;
    std::vector<ResourceMaterialData> materials;
    std::vector<ResourceTextureData> textures;
    std::unordered_map<std::string, ResourceAnimationData> animations;
    std::unordered_map<std::string, std::unordered_map<std::string, ResourceBoneData>> skeletons;
};

struct BoneData {
    unsigned int currentIndex = 0;
    std::vector<float> boneIDs;
    std::vector<float> weights;
    void addData(unsigned int index, float weight){
        weights.push_back(weight);
        boneIDs.push_back(index);
        currentIndex++;
    }
};

struct AssimpBone {
    int index;
    Mat4f offset;
    std::vector<uint32_t> vertexIDs;
    std::vector<float> weights;
};